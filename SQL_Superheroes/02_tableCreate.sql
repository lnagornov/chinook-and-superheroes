USE SuperheroesDb;

CREATE TABLE Superhero(
    Id int IDENTITY(1, 1),
    Name varchar(30),
    Alias varchar(30),
    Origin varchar(30),
	PRIMARY KEY (Id)
);

CREATE TABLE Assistant (
    Id int IDENTITY(1, 1),
    Name varchar(30),
	PRIMARY KEY (Id)
);

CREATE TABLE Power (
    Id int IDENTITY(1, 1),
    Name varchar(30),
    Description varchar(30),
	PRIMARY KEY (Id)
);