-- (1) Create a database - SuperheroesDb and use it inside the context --
CREATE DATABASE SuperheroesDb;

GO
USE SuperheroesDb;

-- (2) Create tables Superhero, Assistant, Power and setup their primary keys  --
CREATE TABLE Superhero(
    Id int IDENTITY(1, 1),
    Name varchar(30),
    Alias varchar(30),
    Origin varchar(30),
	PRIMARY KEY (Id)
);

CREATE TABLE Assistant (
    Id int IDENTITY(1, 1),
    Name varchar(30),
	PRIMARY KEY (Id)
);

CREATE TABLE Power (
    Id int IDENTITY(1, 1),
    Name varchar(30),
    Description varchar(30),
	PRIMARY KEY (Id)
);


-- (3) Add a constraint - foreign key to Assistant table, so the relationship between Superhero and Assistant is One-to-Many --
-- One Superhero can have multiple Assistants, and one Assistant has one Superhero they assist --
ALTER TABLE Assistant
ADD Superhero_Id int FOREIGN KEY REFERENCES Superhero(Id);


-- (4) Create a correlation table of Superhero and Power with 2 foreign keys and a composite primary key, so the relationship between Superhero and Power is Many-to-Many --
-- One Superhero can have many Powers, and one Power can be present on many Superheroes --
CREATE TABLE Superhero_Power (
    SuperheroId int not null,
    PowerId int not null,
);

ALTER TABLE Superhero_Power ADD FOREIGN KEY (SuperheroId) REFERENCES Superhero(Id);
ALTER TABLE Superhero_Power ADD FOREIGN KEY (PowerId) REFERENCES Power(Id);
ALTER TABLE Superhero_Power ADD PRIMARY KEY (SuperheroId, PowerId);


-- (5) Insert three new superheroes into Superhero table --
INSERT INTO Superhero(Name, Alias, Origin)
VALUES ('Deadpool', 'DPool', 'Marvels');

INSERT INTO Superhero(Name, Alias, Origin)
VALUES ('Saitama', 'One Punch', 'One Punch Man');

INSERT INTO Superhero(Name, Alias, Origin)
VALUES ('Wolverine', 'Wolf', 'XMen');


-- (6) Insert three new assistants and the superheroes they assist into Assistant table --
INSERT INTO Assistant(Name, Superhero_Id)
VALUES ('Doctor assistant', 1);

INSERT INTO Assistant(Name, Superhero_Id)
VALUES ('Assistant man', 3);

INSERT INTO Assistant(Name, Superhero_Id)
VALUES ('Assistantos', 2);


-- (7) Insert four new powers into Power table --
INSERT INTO Power(Name, Description)
VALUES ('Power 1','Description 1');

INSERT INTO Power(Name, Description)
VALUES ('Power 2','Description 2');

INSERT INTO Power(Name, Description)
VALUES ('Power 3','Description 3');

INSERT INTO Power(Name, Description)
VALUES ('Power 4','Description 4');

-- Link powers with superheroes to demonstrate Many-to-Many relationship --
-- There is Superhero with Id = 3 that has several powers and there is a one Power with Id = 1 that is used by multiple superheroes --
INSERT INTO Superhero_Power(SuperheroId, PowerId)
VALUES (1, 1);

INSERT INTO Superhero_Power(SuperheroId, PowerId)
VALUES (2, 1);

INSERT INTO Superhero_Power(SuperheroId, PowerId)
VALUES (3, 1);

INSERT INTO Superhero_Power(SuperheroId, PowerId)
VALUES (3, 2);

INSERT INTO Superhero_Power(SuperheroId, PowerId)
VALUES (3, 3);


-- (8) Update a superhero name in Superhero table by Id --
UPDATE Superhero
SET Name = 'Superman'
WHERE Id = 1;


-- (9) Delete an assistant from Assistant table by name --
DELETE FROM Assistant WHERE Name = 'Assistant man';
