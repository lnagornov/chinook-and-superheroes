﻿using DataAccess.Models;
using DataAccess.Utils;
using DataAccess.Repositories;

namespace DataAccess
{
    internal static class Program
    {
        public static void Main(string[] args)
        {
            var customerRepository = new CustomerRepository();
            
            // 1. Read all the customers in the database.
            var allCustomers = customerRepository.GetCustomers();
            ConsolePrinter.PrintCustomers(allCustomers);
            
            
            // 2. Read a specific customer from the database (by Id).
            var customerWithId1 = customerRepository.GetCustomerById(1);
            ConsolePrinter.PrintCustomer(customerWithId1);
            
            
            // 3. Read a specific customer by name.
            var customerWithNameMark = customerRepository.GetCustomerByName("Mark");
            ConsolePrinter.PrintCustomer(customerWithNameMark);
            
            
            // 4. Return a page of customers from the database with limit and offset as parameters.
            var pageOfCustomersWithLimitAndOffset = customerRepository.GetPageOfCustomers(5, 2);
            ConsolePrinter.PrintCustomers(pageOfCustomersWithLimitAndOffset);


            // 5. Add a new customer.
            var newCustomer = new Customer()
            {
                FirstName = "John",
                LastName = "Doe",
                Country = "Netherlands",
                Email = "johndoe@example.com",
                Phone = "+31222333444",
                PostalCode = "ABC123"
            };
            customerRepository.AddCustomer(newCustomer);
            
            
            // 6. Update customer data.
            var updatedCustomer = new Customer()
            {
                Id = 60,
                Country = "USA",
            };
            customerRepository.UpdateCustomer(updatedCustomer);
            
            
            // 7. Return the number of customers in each country, ordered descending (high to low). i.e. USA: 13.
            var customersInEachCountry = customerRepository.GetCustomerCountByCountry();
            ConsolePrinter.PrintCustomersCountry(customersInEachCountry);


            // 8. Customers who are the highest spenders (total in invoice table is the largest), ordered descending.
            var customersTopSpenders = customerRepository.GetCustomersTopSpenders();
            ConsolePrinter.PrintCustomersTopSpenders(customersTopSpenders);
            
            
            // 9. For a given customer, their most popular genre (in the case of a tie, display both).
            // Most popular in this context means the genre that corresponds to the most tracks from invoices associated to that customer.
            var customerTopGenres = customerRepository.GetCustomerTopGenres(1);
            ConsolePrinter.PrintCustomerTopGenres(customerTopGenres);
            
            
            // 10. Delete customer
            customerRepository.DeleteCustomer(60);
        }
    }
}
