﻿using System.Data.SqlClient;

namespace DataAccess.Utils
{
    public static class ConnectionHelper
    {
        /// <summary>
        /// Returns SQL connection string.
        /// </summary>
        /// <returns>Connection string.</returns>
        public static string GetConnectionString()
        {
            var builder = new SqlConnectionStringBuilder
            {
                DataSource = @"LN11NKBZA37007\SQLEXPRESS",  // ! CHANGE this "LN11NKBZA37007" to your local server name !
                InitialCatalog = "Chinook",
                IntegratedSecurity = true,
                TrustServerCertificate = true
            };

            return builder.ConnectionString;
        }
    }
}