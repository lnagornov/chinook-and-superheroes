﻿using System.Collections.Generic;
using DataAccess.Models;

namespace DataAccess.Repositories
{
    public interface ICustomerRepository
    {
        // Create
        void AddCustomer(Customer customer);
        
        // Read
        Customer GetCustomerById(int customerId);
        Customer GetCustomerByName(string customerName);
        IEnumerable<Customer> GetCustomers();
        IEnumerable<Customer> GetPageOfCustomers(int limit, int offset);
        IEnumerable<CustomerSpender> GetCustomersTopSpenders();
        IEnumerable<CustomerCountry> GetCustomerCountByCountry();
        IEnumerable<CustomerGenre> GetCustomerTopGenres(int customerId);
        
        // Update
        void UpdateCustomer(Customer customer);
        
        // Delete
        void DeleteCustomer(int customerId);
    }
}