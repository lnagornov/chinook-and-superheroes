﻿namespace DataAccess.Models
{
    public class CustomerCountry
    {
        public string CountryName { get; set; }
        public int NumberOfCustomers { get; set; }
    }
}